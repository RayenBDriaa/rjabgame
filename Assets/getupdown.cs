using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getupdown : MonoBehaviour
{

    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        anim.SetFloat("updown", Input.GetAxis("Vertical"));

        anim.SetFloat("rightleft", Input.GetAxis("Horizontal"));
        if (Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("tojump");
        }
        if (Input.GetButtonDown("left shift"))
        {
            anim.SetTrigger("runbool");
        }
        if(Input.GetKey("left ctrl"))
        {
            anim.SetTrigger("Crouch");
        }
    }
     void FixedUpdate()
    {
        

    }
}
